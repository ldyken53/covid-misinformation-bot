import re
import tweepy
import json
from tweepy import OAuthHandler

import sys
from google.cloud import storage

def upload_tweets(bucket_name, tweets):
    storage_client = storage.Client()
    bucket = storage_client.bucket(bucket_name)
    f = open("tweets.json", "w")
    f.write(json.dumps(tweets, default=str))
    blob = bucket.blob("New Tweets")
    blob.upload_from_filename("tweets.json")
    print("Tweets uploaded to Google Cloud successfully")

  
bot_message = """
This tweet may contain misleading or incorrect information regarding the COVID-19 vaccine. For facts and more information about the COVID-19 vaccine, please see the following links:
https://www.cdc.gov/coronavirus/2019-ncov/vaccines/facts.html
https://www.cdc.gov/coronavirus/2019-ncov/downloads/vaccines/toolkits/AdditionalFAQ_COVID-19Vaccination-508.pdf
"""

class TwitterClient(object):
    def __init__(self):
        # keys and tokens
        consumer_key = 'qPnTmknIGn3hkj0ux6YNfXRyI'
        consumer_secret = 'yO9iPhQPdUiv6CYk7iJXsyncvAHIWglOldLL8cnoswI2RKTJcl'
        access_token = '1377705448560807941-K6qDP2l4Mx9JeHXMU4MHIWe7iKaSSr'
        access_token_secret = 'eCKfqRYIdsZOC86a7962P3QRZoobkdYhyQ3wzUb4QLKrV'
  
        # authenticate
        try:
            self.auth = OAuthHandler(consumer_key, consumer_secret)
            self.auth.set_access_token(access_token, access_token_secret)
            self.api = tweepy.API(self.auth)
        except:
            print("Error: Authentication Failed")
  
    def get_tweets(self, query, count = 10):
        # empty list to store parsed tweets
        tweets = []
  
        try:
            # call twitter api to fetch tweets
            fetched_tweets = self.api.search(q = query, count = count)
  
            # parsing tweets one by one
            for tweet in fetched_tweets:
                # empty dictionary to store required params of a tweet
                parsed_tweet = {}
  
                # saving text and id of tweet
                parsed_tweet['text'] = tweet.text
                parsed_tweet['id'] = tweet.id
  
                # appending tweets
                if tweet.retweet_count > 0:
                    # if tweet has retweets, ensure that it is appended only once
                    if parsed_tweet not in tweets:
                        tweets.append(parsed_tweet)
                else:
                    tweets.append(parsed_tweet)
  
            return tweets
  
        except tweepy.TweepError as e:
            print("Error : " + str(e))

    def reply_tweet(self, tweet):
        self.api.update_status(bot_message, in_reply_to_status_id = tweet['id'], auto_populate_reply_metadata=True)
        
def main():
    api = TwitterClient()
    # construct queries from list of conspiracy buzzwords
    buzzwords = ["microchip", "autism", "AIDS", "5G", "China virus", "weapon", "brainwash", "mind control", "slaves", "deep state"]
    queries = [f"Covid {buzzword}" for buzzword in buzzwords]

    # grab tweets for all our queries
    tweets = []
    for query in queries:
        new_tweets = api.get_tweets(query=query, count=100)
        print(f"Found {len(new_tweets)} tweets with query {query}")
        tweets.extend(new_tweets)

    print(f"Found {len(tweets)} tweets in all")
    print("Uploading tweets...")
    upload_tweets("covidbottweeter", tweets)
    print("Replying to tweets...")
    for tweet in tweets:
        try:
            api.reply_tweet(tweet)
        except:
            print("Hit duplicate status, skipping reply to tweet...")
    print("Successfully replied to COVID misinformation tweets")

  
if __name__ == "__main__":
    # replies to tweets in the past 7 days with COVID information links
    main()